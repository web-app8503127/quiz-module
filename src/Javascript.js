var ele=document.getElementById("timer");
var seconds=00,hh=00,mm=00,ss=00;
const timing=setInterval(function(){
    seconds=localStorage.getItem("timer");
    if(!localStorage.getItem("timer"))
    {
      seconds=0;
      localStorage.setItem("timer",seconds)
    }  
    seconds++;
    hh=parseInt(seconds/3600);
    mm=parseInt(seconds/60);
    if(mm>59)
      mm%=60;
    ss=seconds%60;
      
    ele.innerHTML="Timer :: "+hh+" hh "+mm+" mm "+ss+" ss";
    localStorage.setItem("timer",seconds);
    },1000);

let element1=document.createElement("div");
document.getElementById("container").appendChild(element1);
element1.setAttribute("id","text");
var score=0;
var questions=[{
  title: "1. A complete graph can have",
  options:["n2 spanning trees","nn - 2 spanning trees","nn - 2 spanning trees","nn - 2 spanning trees"],
  correct: "n2 spanning trees",
  marks:1
},
{
  title: "2. What is full form of CSS",
  options: ["Javascript","Java","HTML","None of these"],
  correct: "None of these",
  marks: 1
},
{
  title: "3. Which alphabet is A",
  options: ["A","D","B","C"],
  correct: "A",
  marks: 1
},
{
  title: "4. Which number is 4",
  options: [10,22,8,4],
  correct: 4,
  marks: 1
},
{
  title: "5. Which is alphabet R",
  options: ["X","R","N","S"],
  correct: "R",
  marks: 1
},
{
  title: "6. Which is equal to 4*5",
  options: [36,20,45,54],
  correct: 20,
  marks: 1
},
{
  title: "7. Which is equal to 8+9",
  options: [89,17,98,45],
  correct: 17,
  marks: 1 
},
{
  title: '8. Ans of "8"+3',
  options: ['"38"','"83"','11','"11"'],
  correct: '"83"',
  marks: 1
},
{
  title: "9. Answer of 7*6",
  options: [67,42,76,24],
  correct: 42,
  marks: 1
},
{
  title: "10. Which is alphabet S",
  options: ["W","S","J","T"],
  correct: "S",
  marks: 1
}
];
let ob;
function makeRadioButton() 
{            
  ob=localStorage.getItem("index");
  if(!localStorage.getItem("index"))
  {
    localStorage.setItem("index","0");
    ob=0;
  }
  let d=document.createElement("div");
  d.setAttribute("id","f1");
  let Q1=document.createElement("p");
  Q1.innerHTML= questions[ob].title;
  d.appendChild(Q1);  
  for (var i = 0; i < questions[ob].options.length; i++) 
  {
    let l1 = document.createElement("label");
    let r1 = document.createElement("input");
    r1.type = "radio";
    r1.name = questions[ob].title;
    r1.value = questions[ob].options[i];
    l1.innerHTML=questions[ob].options[i] + "<br>";
    d.appendChild(r1);
    d.appendChild(l1);
  }
  let subbtn=document.createElement("input");
  subbtn.type="submit";
  subbtn.value="Submit";
  subbtn.id="send";
  subbtn.addEventListener("click",result);
  d.appendChild(subbtn);
  element1.append(d);
}
makeRadioButton();
function result(){
  score=localStorage.getItem("score");
  if(!localStorage.getItem("score"))
  {
    score=0;
    localStorage.setItem("score",score);
  }
  var ans,opt;
  ans=document.getElementsByTagName("input");
  for(let i=0;i<ans.length-1;i++)
  {
    if(ans[i].checked)
    {
      opt=ans[i];
      break;
    }  
  }
  if(!opt)
  {
    alert('Please select atleast one option!');
    return;
  }
  ans[4].style.display="none";
  for(let i=0;i<ans.length-1;i++)
    ans[i].disabled=true;
  var feed=document.createElement("p");
  feed.setAttribute("id","feed");
  element1.appendChild(feed);
  if(opt.value==questions[ob].correct)
  {
    feed.innerHTML="Correct";
    feed.style.backgroundColor="rgb(31, 241, 31)";
    score++;
  }  
  else
  {
    feed.innerHTML="Incorrect";
    feed.style.backgroundColor="rgba(245, 8, 8, 0.781)";
  }
  ob++;
  localStorage.setItem("index",ob);
  localStorage.setItem("score",score);
  var next=document.createElement("button");
  next.setAttribute("id","next");
  if(ob==10)
    next.innerHTML="View score";
  else
    next.innerHTML="Next";  
  element1.appendChild(next);
  next.addEventListener("click",nextquestion);   
}
function nextquestion(){
  element1.removeChild(document.getElementById("feed"));
  element1.removeChild(document.getElementById("next"));
  if(ob<10)
  {
    let p=document.getElementById("f1");
    element1.removeChild(p);
    makeRadioButton();
  }  
  else
  {
    clearInterval(timing);
    let final=document.createElement("div");
    final.setAttribute("id","fin");
    final.innerHTML="Your score is "+score +" out of 10";
    document.body.removeChild(document.getElementById("container"));
    document.body.appendChild(final);
    if(score<7 && score>3)
      final.style.backgroundColor="#ffd633";
    else if(score<4)
      final.style.backgroundColor="#ff6666";

    answerkey();
  }
}
function answerkey(){
  let div=document.createElement("div");
  div.setAttribute("class","fintext");
  let para=document.createElement("p");
  para.innerHTML="Answer Key :-";
  div.appendChild(para);
  questions.forEach(function(x){
    let p=document.createElement("p");
    p.innerHTML=x.title+" = "+x.correct+"<br>";
    para.appendChild(p);
  })
  document.body.appendChild(div);
  let anchor=document.createElement("a");
  anchor.innerHTML="Restart Quiz";
  anchor.setAttribute("href","http://127.0.0.1:5500/QuizModule/");
  document.body.appendChild(anchor);
  localStorage.clear();
}